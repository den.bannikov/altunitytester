package ro.altom.altunitytester.altUnityTesterExceptions;

public class PropertyCannotBeSetException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = 7273081127942225690L;

    public PropertyCannotBeSetException() {
    }

    public PropertyCannotBeSetException(String message) {
        super(message);

    }
}
