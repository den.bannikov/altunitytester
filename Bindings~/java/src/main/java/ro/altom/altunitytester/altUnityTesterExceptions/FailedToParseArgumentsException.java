package ro.altom.altunitytester.altUnityTesterExceptions;

public class FailedToParseArgumentsException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = 6034597081437314046L;

    public FailedToParseArgumentsException() {
    }

    public FailedToParseArgumentsException(String message) {
        super(message);
    }
}
