package ro.altom.altunitytester.altUnityTesterExceptions;

public class NullReferenceException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = 11068361256672838L;

    public NullReferenceException() {
    }

    public NullReferenceException(String message) {
        super(message);
    }
}
