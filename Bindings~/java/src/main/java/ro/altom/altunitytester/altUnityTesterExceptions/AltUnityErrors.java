package ro.altom.altunitytester.altUnityTesterExceptions;

public class AltUnityErrors {
    public static final String errorNotFoundMessage = "notFound";
    public static final String errorPropertyNotFoundMessage = "propertyNotFound";
    public static final String errorMethodNotFoundMessage = "methodNotFound";
    public static final String errorComponentNotFoundMessage = "componentNotFound";
    public static final String errorAssemblyNotFoundMessage = "assemblyNotFound";
    public static final String errorCouldNotPerformOperationMessage = "couldNotPerformOperation";
    public static final String errorCouldNotParseJsonString = "couldNotParseJsonString";
    public static final String errorMethodWithGivenParametersNotFound = "methodWithGivenParametersNotFound";
    public static final String errorInvalidParameterType = "invalidParameterType";
    public static final String errorFailedToParseArguments = "failedToParseMethodArguments";
    public static final String errorObjectWasNotFound = "objectNotFound";
    public static final String errorPropertyNotSet = "propertyCannotBeSet";
    public static final String errorNullReferenceMessage = "nullReferenceException";
    public static final String errorUnknownError = "unknownError";
    public static final String errorFormatException = "formatException";
    public static final String errorCameraNotFound = "cameraNotFound";
    public static final String errorIndexOutOfRange = "indexOutOfRange";
    public static final String errorInvalidParametersOnDriverCommand = "invalidParametersOnDriverCommand";
    public static final String errorInvalidCommand = "invalidCommand";
    public static final String errorInvalidPath = "invalidPath";
    public static final String errorInputModule = "ALTUNITYTESTERNotAddedAsDefineVariable";
}
