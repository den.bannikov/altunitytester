package ro.altom.altunitytester.altUnityTesterExceptions;

public class AltUnityInputModuleException extends AltUnityException {
    private static final long serialVersionUID = 4896864591826296908L;

    public AltUnityInputModuleException() {
    }

    public AltUnityInputModuleException(String message) {
        super(message);
    }

}
