package ro.altom.altunitytester.altUnityTesterExceptions;

public class InvalidCommandException extends AltUnityException {
    
    private static final long serialVersionUID = 3793304868452564746L;

    public InvalidCommandException(String message) {
        super(message);
    }
}
